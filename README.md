# drpl.refreshless.sebastix.dev

## RefreshLess



## Docker

See `docker/docker-compose.yml`.

## CI/CD

See `.gitlab-ci.yml`

## Start development

1. `cd docker`
2. `docker compose up -d`
3. `docker compose exec drplrefresh_drupal bash`
4. `composer install`
5. `vendor/bin/drush si standard` for a clean site install
6. Delete existing entities
   * `vendor/bin/drush entity-delete shortcut` delete shortcut entities
7. Enable devel module `vendor/bin/drush en devel`
8. Generate a new UUID for the site `vendor/bin/drush uuid`
9. Save this site UUID value site in `system.site.yml`
10. `vendor/bin/drush cset system.site uuid <the_uuid>` for setting the UUID in the database
11. `vendor/bin/drush cim` for importing current config files
12. `vendor/bin/drush cr`
13. Navigate to https://localhost in your browser

## Updates

1. `composer outdated`
2. `composer update -W`
3. `vendor/bin/drush cr`
4. `vendor/bin/drush updb`
5. `vendor/bin/drush cex`
6. `composer clearcache`

## What modules are included?

* AdvAgg
* Config split
* Drush
* Raven
* Backup Migrate
* Symfony Mailer
* Paragraphs
* Admin Toolbar
* Gin
* Admin Dialogs
* Pathauto
* Masquerade
* Ultimate Cron
* Advanced CSS/JS Aggregation
* Media Library Edit

Development only:
* Coder
* Devel
* Webprofiler
* Drupal Coder
* Drupal Rector

### Config split configurations

```php
$config['config_split.config_split.dev']['status'] = TRUE|FALSE;
$config['config_split.config_split.acceptance']['status'] = TRUE|FALSE;
$config['config_split.config_split.production']['status'] = TRUE|FALSE;
```
`settings.local.php`

#### Production

#### Acceptance

#### Development

## Security checks

@TODO - https://github.com/FriendsOfPHP/security-advisories and https://github.com/fabpot/local-php-security-checker

## Code checks

@TODO - https://www.drupal.org/project/coder
